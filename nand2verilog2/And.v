`include "Nand.v"
module And (
    input a, b,
    output out,
    output wifi_gpio0
);

    wire out_1;

    Nand nand_1 (.a (a), .b (b), .out (out_1));
    
    Nand nand_2 (
        .a      ( out_1 ),
        .b      ( out_1 ),
        .out    ( out )
    );

    assign wifi_gpio0 = 1'b1;
endmodule