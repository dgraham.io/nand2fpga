# hack_fpga_cpu

FPGA implementation of Hack CPU componnents detailed in "The Elements of Computing Systems"

## Getting Started

```
yosys And.ys
nextpnr-ecp5 --85k --json And.json --lpf ../ulx3s_v20.lpf --textcfg ulx3s_out.config
ecppack ulx3s_out.config ulx3s.bit
fujprog ulx3s.bit
```