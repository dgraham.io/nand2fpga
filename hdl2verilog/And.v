module And (
    input a, b,
    output out
);
    wire out_1;
  
    assign out_1 = a ~& b;
    assign out = out_1 ~& out_1; 
endmodule